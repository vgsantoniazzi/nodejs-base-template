require('./test_helper.js')

before(function(){
  app.models.example.create({example: 'vgsantoniazzi'})
});

it('display a login page', function(done) {
  request.get('/').end(function(err, response) {
    response.status.should.eql(200);
    done();
  });
});


it('#findByEmail', function(done) {
  app.models.example.findByEmail('vgsantoniazzi' , function(err, doc){
    doc.example.should.equal('vgsantoniazzi');
    done();
  });
});

it('create a new user', function(done) {
  request.post('/login')
    .send({"email": "test@gmail.com"})
    .end(function(err, response) {
      response.text.should.eql("created\n");
      done();
  });
});


describe('create and log user', function(){
  before('create a new user and make it logged', function(done) {
    request.post('/login')
      .send({"email": "test1@test1.com"})
      .end(function(err, response) {
        response.text.should.eql("created\n");
        done();
    });
    request.post('/login')
      .send({"email": "test1@test1.com"})
      .end(function(err, response) {
        response.text.should.eql("logged\n");
        done();
    });
  });
});
