global.app = require('../app.js');
global.should = require('should');
global.mongoose = require('mongoose');
global.request = require('supertest')(app);

after(function (done) {
  db.connection.db.dropDatabase(function(){
    done();
  });
});
