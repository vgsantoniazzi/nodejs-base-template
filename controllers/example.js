module.exports = function(app){
  var Example = app.models.example;

  var ExampleController = {
    index: function(request, response){
      response.render('example/index');
    },

    login: function(request, response){
      Example.findOne({example: request.body.email})
        .select('example')
        .exec(function(err, example){
          if(example){
            response.render('example/login',{
              status: 'logged'
            });
          } else {
            Example.create({example: request.body.email}, function(err, example){
              if(example) {
                response.render('example/login',{
                  status: 'created'
                });
              } else {
                response.render('example/login',{
                  status: 'error'
                });
              }
            });
          }
        });
    },
  };
  return ExampleController;
};
