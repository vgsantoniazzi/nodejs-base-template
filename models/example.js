module.exports = function(app) {
  var Schema = require('mongoose').Schema;

  var ExampleSchema = new Schema({
    example: String
  });

  ExampleSchema.statics.findByEmail = function (email, cb) {
    this.findOne({ example: email }, cb);
  }

  return db.model('Example', ExampleSchema);
};
