module.exports = function(app){
  var example = app.controllers.example;
  app.get('/', example.index);
  app.post('/login', example.login);
}
